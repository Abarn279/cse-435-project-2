$(function () {
    doHeightCalculations();

    $(window).resize(function () {
        doHeightCalculations();
    });

    $('#userInfo').modal({
        backdrop: 'static',
        keyboard: false
    });
});

function doHeightCalculations() {
    $("body").css("height", $(window).height());
    $(".fill").css("height", $(window).height() - $("#fixedTop").outerHeight() - 15);
}
