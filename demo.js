var demoApp = angular.module('demoApp', []);

demoApp.controller('demoCtrl', function ($scope) {
    $scope.userInfo = {
        startTime: null,
        endTime: null,
        shift: null,
        name: null,
        plant: null,
		station: null,
        isComplete: function () {
            return this.shift !== null && this.name !== null && this.plant !== null && this.station !== null;
        }
    }

    $scope.userInfoChoices = {
        shifts: {
            A: "A",
            B: "B",
            C: "C"
        },
        plants: {
            LDT: "Lansing Delta Township",
            LGR: "Lansing Grand River",
            LO: "Lake Orion"
        },
		stations: {
			ECoat: "E-coat",
			Prime: "Prime Coat",
			Top: "Top Coat"
		}
    }

    $scope.saveUserChanges = function () {
        if ($scope.userInfo.isComplete()) {
            $('#userInfo').modal('hide');
            $scope.userInfo.startTime = new Date();
        }
    }

    $scope.models = {
        ATS: "ATS",
        CTS4: "CTS4"
    }

    $scope.surfaces = {
        LEFT: "Left",
        RIGHT: "Right",
        TOP: "Top"
    }

    $scope.defectTypes = {
        COTTONFIBER: {
            type: 1,
            color: "red"
        },
        CRATER: {
            type: 2,
            color: "blue"
        },
        EGGSHELL: {
            type: 3,
            color: "yellow"
        },
        FIBER: {
            type: 4,
            color: "green"
        }
    }

    $scope.defectType = $scope.defectTypes.COTTONFIBER;

    $scope.selectedOptions = {
        model: null,
        surface: null,

        setSurface: function (surface) {
            this.surface = surface;
        }
    }

    $scope.severity = 1;

    //Are there unsaved changes to a part?
    $scope.currentChanges = false;
    //Is the part we are currently working on a new part, or a historic part?
    $scope.isNewPart = false;
    //Is there an active session?
    $scope.activeSession = true;

    $scope.showPicker = function () {
        return !$scope.isActivePart();
    };

    $scope.history = [];

    $scope.activePart = {};

    $scope.isActivePart = function () {
        //if active part isn't empty, there is an active part.
        return Object.keys($scope.activePart).length > 0;
    }

    $scope.createNewPart = function () {
        if ($scope.currentChanges) {
            if (confirm("There are current changes. Erase and proceed?")) $scope.cleanUp();
        } else $scope.cleanUp();
    }

    $scope.createPart = function () {
        if ($scope.selectedOptions.model && $scope.selectedOptions.surface) {
            $scope.activePart = new Part($scope.selectedOptions.model, $scope.selectedOptions.surface);
            $scope.isNewPart = true;
            $scope.selectedOptions.model = null;
            $scope.selectedOptions.surface = null;
        } else alert("Please select a model and surface!")
    }

    $scope.addPart = function () {
        if ($scope.activePart.defects.length > 0) {
            $scope.history.unshift($scope.activePart);
            $scope.isNewPart = false;
            $scope.cleanUp();
        } else alert("No defects listed!");
    }

    $scope.updatePart = function (index) {
        var index = $scope.activePart.index;
        delete $scope.activePart.index;
        $scope.history[index] = $scope.activePart;
        $scope.cleanUp();
    }

    $scope.removeMarks = function () {
        $(".defect").remove();
    }

    $scope.cleanUp = function () {
        $scope.activePart = {};
        $scope.currentChanges = false;
        $scope.removeMarks();
    }

    $scope.createDefect = function (e) {
        var x = e.pageX - e.target.x;
        var y = e.pageY - e.target.y;
        var defect = new Defect(x, y, $scope.severity, $scope.defectType);
        $scope.currentChanges = true;
        $scope.activePart.defects.push(defect);
        $scope.drawDefects([defect], "#wireframeHolder");
    }

    $scope.drawDefects = function (arrayOfDefects, canvasToDrawTo) {
        var x, defect;
        for (var i = 0; i < arrayOfDefects.length; i++) {
            x = $("<div class='defect circle'></div>");
            defect = arrayOfDefects[i];
            x.css("background-color", defect.type.color)
                .css("top", defect.y)
                .css("left", defect.x)
                .css("width", defect.severity * 5)
                .css("height", defect.severity * 5);
            $(canvasToDrawTo).append(x);
        }
    }

    $scope.editHistoryItem = function (index) {
        if (!$scope.currentChanges || confirm("There are unsaved changes! Proceed?")) {
            $scope.cleanUp();
            $scope.activePart = jQuery.extend(true, {}, $scope.history[index]);
            $scope.activePart.index = index;
            $scope.drawDefects($scope.activePart.defects, "#wireframeHolder");
        }
    }

    $scope.getNumberOfDefects = function () {
        var defects = 0;
        for (var i = 0; i < $scope.history.length; i++) {
            defects += $scope.history[i].defects.length;
        }
        return defects;
    }

    $scope.endSession = function () {
        $scope.userInfo.endTime = new Date();
        $scope.cleanUp();
        $scope.activeSession = false;

        var history = $scope.history;
        var left = [],
            top = [],
            right = [];
        for (var i = 0; i < history.length; i++) {
            if (history[i].surface === $scope.surfaces.LEFT) left = left.concat(history[i].defects)
            if (history[i].surface === $scope.surfaces.TOP) top = top.concat(history[i].defects)
            if (history[i].surface === $scope.surfaces.RIGHT) right = right.concat(history[i].defects)
        }
        $scope.drawDefects(left, "#wireframeHolderLeft");
        $scope.drawDefects(top, "#wireframeHolderTop");
        $scope.drawDefects(right, "#wireframeHolderRight");

    }

    var Part = function (model, surface) {
        return {
            model: model,
            surface: surface,
            defects: []
        };
    }

    var Defect = function (x, y, severity, type) {
        return {
            x: x,
            y: y,
            severity: severity,
            type: type
        };
    }

    $scope.loadTestData = function () {
        $scope.cleanUp();
        $scope.history = new TestData();
    }

    //null for either argument indicates that you want all levels/parts
    $scope.getNumberOfDefectsBySeverityAndPart = function (level, part) {
        var total = 0;
        var history = $scope.history;

        if (part === null && level === null) alert("Not Implemented");

        else if (part === null) {
            for (var i = 0; i < history.length; i++) {
                for (var j = 0; j < history[i].defects.length; j++) {
                    if (history[i].defects[j].severity === level) total += 1;
                }
            }
        } else if (level === null) {

            for (var i = 0; i < history.length; i++) {
                for (var j = 0; j < history[i].defects.length; j++) {
                    if (history[i].defects[j].section === part) total += 1;
                }
            }
        } else {
            for (var i = 0; i < history.length; i++) {
                for (var j = 0; j < history[i].defects.length; j++) {
                    if (history[i].defects[j].section === part && history[i].defects[j].severity === level) total += 1;
                }
            }
        }

        return total;
    }
});

var TestData = function () {
    return [
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 586,
                    "y": 94,
                    "severity": 1,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 543,
                    "y": 93,
                    "severity": 1,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 391,
                    "y": 185,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 367,
                    "y": 182,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 350,
                    "y": 185,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 428,
                    "y": 183,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 431,
                    "y": 110,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 485,
                    "y": 116,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 435,
                    "y": 148,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 156,
                    "y": 84,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 179,
                    "y": 113,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 147,
                    "y": 101,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 397,
                    "y": 13,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 504,
                    "y": 31,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 524,
                    "y": 46,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 569,
                    "y": 94,
                    "severity": 5,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 603,
                    "y": 114,
                    "severity": 5,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 590,
                    "y": 84,
                    "severity": 5,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 437,
                    "y": 118,
                    "severity": 3,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 426,
                    "y": 105,
                    "severity": 3,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 423,
                    "y": 114,
                    "severity": 3,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 453,
                    "y": 101,
                    "severity": 3,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 251,
                    "y": 117,
                    "severity": 3,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 286,
                    "y": 128,
                    "severity": 3,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 341,
                    "y": 139,
                    "severity": 3,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 307,
                    "y": 103,
                    "severity": 3,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 166,
                    "y": 102,
                    "severity": 3,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 183,
                    "y": 124,
                    "severity": 3,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 145,
                    "y": 104,
                    "severity": 3,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Left",
            "defects": [
                {
                    "x": 151,
                    "y": 89,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 164,
                    "y": 118,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 172,
                    "y": 91,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "LeftVertical"
         },
                {
                    "x": 184,
                    "y": 127,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "LeftVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 570,
                    "y": 189,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "Deck"
         },
                {
                    "x": 581,
                    "y": 162,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "Deck"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 574,
                    "y": 161,
                    "severity": 1,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "Deck"
         },
                {
                    "x": 571,
                    "y": 188,
                    "severity": 1,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "Deck"
         },
                {
                    "x": 574,
                    "y": 85,
                    "severity": 1,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "Deck"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 587,
                    "y": 186,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Deck"
         },
                {
                    "x": 571,
                    "y": 153,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Deck"
         },
                {
                    "x": 592,
                    "y": 111,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Deck"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 569,
                    "y": 98,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "Deck"
         },
                {
                    "x": 583,
                    "y": 125,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "Deck"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 98,
                    "y": 100,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "Hood"
         },
                {
                    "x": 123,
                    "y": 67,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "Hood"
         },
                {
                    "x": 108,
                    "y": 126,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "Hood"
         },
                {
                    "x": 116,
                    "y": 173,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "Hood"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 87,
                    "y": 199,
                    "severity": 4,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "Hood"
         },
                {
                    "x": 132,
                    "y": 202,
                    "severity": 4,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "Hood"
         },
                {
                    "x": 130,
                    "y": 59,
                    "severity": 4,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "Hood"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 55,
                    "y": 82,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Hood"
         },
                {
                    "x": 65,
                    "y": 109,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Hood"
         },
                {
                    "x": 94,
                    "y": 145,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Hood"
         },
                {
                    "x": 119,
                    "y": 115,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Hood"
         },
                {
                    "x": 126,
                    "y": 188,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Hood"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 348,
                    "y": 146,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Roof"
         },
                {
                    "x": 427,
                    "y": 154,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Roof"
         },
                {
                    "x": 408,
                    "y": 181,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Roof"
         },
                {
                    "x": 410,
                    "y": 107,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "Roof"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 289,
                    "y": 88,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "Roof"
         },
                {
                    "x": 372,
                    "y": 122,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "Roof"
         },
                {
                    "x": 326,
                    "y": 132,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "Roof"
         },
                {
                    "x": 414,
                    "y": 135,
                    "severity": 1,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "Roof"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Top",
            "defects": [
                {
                    "x": 283,
                    "y": 192,
                    "severity": 4,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "Roof"
         },
                {
                    "x": 443,
                    "y": 190,
                    "severity": 4,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "Roof"
         },
                {
                    "x": 279,
                    "y": 70,
                    "severity": 4,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "Roof"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 99,
                    "y": 103,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 208,
                    "y": 60,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 472,
                    "y": 112,
                    "severity": 4,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 545,
                    "y": 142,
                    "severity": 4,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 427,
                    "y": 85,
                    "severity": 3,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 204,
                    "y": 43,
                    "severity": 3,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 404,
                    "y": 80,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 309,
                    "y": 58,
                    "severity": 1,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 70,
                    "y": 83,
                    "severity": 2,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 190,
                    "y": 41,
                    "severity": 2,
                    "type": {
                        "type": 4,
                        "color": "green"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 589,
                    "y": 72,
                    "severity": 2,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 446,
                    "y": 38,
                    "severity": 2,
                    "type": {
                        "type": 2,
                        "color": "blue"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 259,
                    "y": 84,
                    "severity": 2,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 152,
                    "y": 101,
                    "severity": 2,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 584,
                    "y": 92,
                    "severity": 2,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 419,
                    "y": 78,
                    "severity": 2,
                    "type": {
                        "type": 3,
                        "color": "yellow"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 443,
                    "y": 96,
                    "severity": 5,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 331,
                    "y": 71,
                    "severity": 5,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "RightVertical"
         }
      ],
   },
        {
            "model": "ATS",
            "surface": "Right",
            "defects": [
                {
                    "x": 294,
                    "y": 41,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "RightVertical"
         },
                {
                    "x": 376,
                    "y": 98,
                    "severity": 1,
                    "type": {
                        "type": 1,
                        "color": "red"
                    },
                    "section": "RightVertical"
         }
      ],
   }
]

}
